#! /usr/bin/env bash

cd ~ || exit
mkdir docker_tmpfiles && cd docker_tmpfiles || exit
git clone https://github.com/keycloak/keycloak-containers.git
cd keycloak-containers/server || exit
docker build -t git-auth-over-https-keycloak .
cd ~ || exit
cd git-auth-over-https || exit
docker-compose up -d keycloak

