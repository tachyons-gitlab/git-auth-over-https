#! /usr/bin/env bash

GIT_ASKPASS=usr/local/bin/device_auth.go

export GIT_ASKPASS
export OAUTH_ID
export CODE_ENDPOINT
export TOKEN_ENDPOINT
